sap.ui.define(["sap/ui/core/mvc/Controller"], function (Controller) {
	"use strict";
	return Controller.extend("QuickStartApplication2.controller.Abostop_4", {
		/**
	*@memberOf QuickStartApplication2.controller.Abostop_4
	*/
submitInterruption: function () { //Unterbrechung hinzufügen
           var dateFrom =  this.getView().byId("datePickerFrom").getDateValue();
           var dateTo =  this.getView().byId("datePickerTo").getDateValue();
           
        if (dateFrom == null) {
                 jQuery.sap.require("sap.m.MessageBox");
        sap.m.MessageBox.show(
              "Bitte Beginn der Unterbrechung eintragen oder das richtige Format beachten", {
                  icon: sap.m.MessageBox.Icon.ERROR,
                  title: "Achtung",
                  actions: [sap.m.MessageBox.Action.OK]  
              }
            );
           }else if (dateTo == null) {
                                jQuery.sap.require("sap.m.MessageBox");
          sap.m.MessageBox.show(
              "Bitte Ende der Unterbrechung eintragen oder das richtige Format beachten", {
                  icon: sap.m.MessageBox.Icon.ERROR,
                  title: "Achtung",
                  actions: [sap.m.MessageBox.Action.OK]  
              }
            );
           }else if (dateFrom >= dateTo) {
                                               jQuery.sap.require("sap.m.MessageBox");
          sap.m.MessageBox.show(
              "Das Beginndatum muss kleiner als das Enddatum sein", {
                  icon: sap.m.MessageBox.Icon.ERROR,
                  title: "Achtung",
                  actions: [sap.m.MessageBox.Action.OK]  
              }
            );
           }
}

	});
});