sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/Device"
], function(JSONModel, Device) {
	"use strict";

	return {

		createDeviceModel: function() {
			var oModel = new JSONModel(Device);
			oModel.setDefaultBindingMode("OneWay");
			return oModel;
		},

		createAboModel: function() {

			var oModel = new sap.ui.model.odata.v2.ODataModel("/destinations/AboOData");
			return oModel;
		},

		createLoginModel: function() {
			var oModel = new JSONModel({
				"bupa": "fgdgdf",
				"plz": "dfgdfg"
			});

			return oModel;
		}

	};

});